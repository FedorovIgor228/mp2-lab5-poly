# Лабораторная работа №5: Полиномы

## Цели и задачи

В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:

- ввод полинома
- организация хранения полинома
- удаление введенного ранее полинома;
- копирование полинома;
- сложение двух полиномов;
- вычисление значения полинома при заданных значениях переменных;
- вывод.

В качестве структуры хранения используются списки. В качестве дополнительной цели в лабораторной работе ставится также задача разработки общего представления списков и операций по их обработке. В числе операций над списками реализованы следующие действия:

- поддержка понятия текущего звена;
- вставка звеньев в начало, после текущей позиции и в конец списков;
- удаление звеньев в начале и в текущей позиции списков;
- организация последовательного доступа к звеньям списка (итератор).

### Условия и ограничения
При выполнении лабораторной работы использовались следующие основные предположения:

1. Разработка структуры хранения должна быть ориентирована на представление полиномов от трех неизвестных.
2. Степени переменных полиномов не могут превышать значения 9.
3. Число мономов в полиномах существенно меньше максимально возможного количества (тем самым, в структуре хранения должны находиться только мономы с ненулевыми коэффициентами).

### План работы
1. Разработка структуры хранения списков.
2. Разработка структуры хранения полиномов.
3. Проверка работоспособности написанных классов с помощью Google Test Framework.
4. Создание GUI и класса для взаимодействия с ним.


### Используемые инструменты
- Система контроля версий **Git**.
- Фреймворк для написания автоматических тестов **Google Test**.
- Среда разработки **QT Creator**.


## Выполнение работы

### 1. Общая структура классов:

![](classes.png)

* TDatValue - абстрактный класс объектов-значений списка
* TMonom - класс мономов

* TRootLink - базовый класс для звеньев
* TDatLink - класс для звеньев (элементов) списка с указателем на объект-значение
* TDatList - класс линейных списков
* THeadRing - класс циклических списков с заголовком
* TPolinom - класс полиномов

Для работы со списками должны быть реализованы следующие операции:
-	методы получения параметров состояния списка (проверка на пустоту, получение текущего количества звеньев);
-	метод доступа к значению первого, текущего или последнего звена;
-	методы навигации по списку (итератор);
-	методы вставки перед первым, после текущего и последнего звеньев;
-	методы удаления первого и текущего звена.

Для работы с полиномами должны быть реализованы следующие операции:
-	конструкторы инициализации и копирования;
-	метод присваивания;
-	метод сложения полиномов.
-	метод сравнения полиномов.


### 2. Абстрактный класс TDatValue.

#### DatValue.h:

    class TDatValue {
	public:
		virtual TDatValue * GetCopy() = 0; // создание копии
		~TDatValue() {}
	};

	typedef TDatValue* PTDatValue;


#### Monom.h:

	#pragma once

    #include "ostream"
    #include"tdatlink.h"

    class TMonom;
    typedef TMonom* PTMonom;

    class TMonom : public TDatValue  {
    protected:
        int Coeff; // коэффициент монома
        int Index; // индекс (свертка степеней)
      public:
        TMonom ( int cval=1, int ival=0 ) {
          Coeff=cval;
          Index=ival;
        }
        virtual TDatValue * GetCopy() // изготовить копию
        {
            TDatValue *temp = new TMonom(Coeff,Index);
            return temp;
        }
        void SetCoeff(int cval) {
            Coeff=cval;
        }
        int  GetCoeff(void)     {
            return Coeff;
        }
        void SetIndex(int ival) {
            Index=ival;
        }
        int  GetIndex(void)     {
            return Index;
        }
        TMonom& operator=(const TMonom &tm) {
          Coeff=tm.Coeff; Index=tm.Index;
          return *this;
        }
        bool operator==(const TMonom &tm) {
          return (this->Coeff==tm.Coeff) && (this->Index==tm.Index);
        }
        int operator<(const TMonom &tm) {
          return Index<tm.Index;
        }
        friend std::ostream& operator <<(std::ostream &os,TMonom &tm){
            os << tm.Coeff << " " << tm.Index;
            return os;
        }
        friend class TPolinom;
    };

#### RootLink.h:

    #ifndef TROOTLINK_H
    #define TROOTLINK_H

    #include"tdatvalue.h"
    #include<iostream>

    class TRootLink;
    typedef TRootLink *PTRootLink;


    class TRootLink {
      protected:
        PTRootLink  pNext;  // указатель на следующее звено
      public:
        TRootLink ( PTRootLink pN = nullptr ) {
            pNext = pN;
        }
        PTRootLink  GetNextLink () {
            return  pNext;
        }
        void SetNextLink ( PTRootLink  pLink ) {
            pNext  = pLink;
        }
        void InsNextLink ( PTRootLink  pLink ) {
          PTRootLink ptr = pNext;
          pNext  = pLink;
          if (pLink != nullptr)
              pLink->pNext = ptr;
        }
        virtual void SetDatValue ( PTDatValue pVal ) = 0;
        virtual PTDatValue GetDatValue () = 0;

        friend class TDatList;
    };

    #endif // TROOTLINK_H


#### DatLink.h:

	#pragma once

    #include"trootlink.h"
    #include"tdatvalue.h"

    class TDatLink;
    typedef TDatLink *PTDatLink;

    class TDatLink : public TRootLink {
      protected:
        PTDatValue pValue;  // указатель на объект значения
      public:
        TDatLink ( PTDatValue pVal = nullptr, PTRootLink pN = nullptr ):TRootLink(pN) {
          pValue = pVal;
        }
        void SetDatValue( PTDatValue pVal ) {
            pValue = pVal;
        }
        PTDatValue GetDatValue() {
            return pValue;
        }
        PTDatLink  GetNextDatLink() {
            return  (PTDatLink)pNext;
        }
        friend class TDatList;
    };

#### DatList.h:

    #pragma once

    #include"tdatlink.h"

    enum TLinkPos {FIRST,CURRENT,LAST};

    class TDatList;
    typedef TDatList * PTDatList;

    class TDatList{
      protected:
        PTDatLink pFirst;    // первое звено
        PTDatLink pLast;     // последнее звено
        PTDatLink pCurrLink; // текущее звено
        PTDatLink pPrevLink; // звено перед текущим
        PTDatLink pStop;     // значение указателя, означающего конец списка
        int CurrPos;         // номер текущего звена (нумерация от 0)
        int ListLen;         // количество звеньев в списке

        // методы
        PTDatLink GetLink ( PTDatValue pVal=nullptr, PTDatLink pLink=nullptr );
        void DelLink ( PTDatLink pLink );   // удаление звена
      public:
        TDatList();
        ~TDatList() {
            DelList();
        }

        // доступ
        PTDatValue GetDatValue ( TLinkPos mode = CURRENT ) const; // значение
        virtual int IsEmpty()  const {
            return pFirst==pStop;
        } // список пуст ?
        int GetListLength()    const {
            return ListLen;
        } // к-во звеньев

        // навигация
        int SetCurrentPos ( int pos );          // установить текущее звено
        int GetCurrentPos () const;       // получить номер тек. звена
        virtual int Reset ();             // установить на начало списка
        virtual int IsListEnded () const; // список завершен ?
        int GoNext ();                    // сдвиг вправо текущего звена
                    // (=1 после применения GoNext для последнего звена списка)

        // вставка звеньев
        virtual void InsFirst  ( PTDatValue pVal=nullptr ); // перед первым
        virtual void InsLast   ( PTDatValue pVal=nullptr ); // вставить последним
        virtual void InsCurrent( PTDatValue pVal=nullptr ); // перед текущим

        // удаление звеньев и списка
        virtual void DelFirst  ();    // удалить первое звено
        virtual void DelCurrent();    // удалить текущее звено
        virtual void DelList();       // удалить весь список
    };



#### DatList.cpp:

	#include "tdatlist.h"

    PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
    {
        return new TDatLink(pVal, pLink);
    }

    void TDatList::DelLink(PTDatLink pLink)
    {
        if (pLink != nullptr)
        {
            if (pLink->pValue != nullptr)
                delete pLink->pValue;
            delete pLink;
        }
    }

    TDatList::TDatList(): ListLen(0)
    {
        pFirst = pLast = pStop = nullptr;
        Reset();
    }

    PTDatValue TDatList::GetDatValue(TLinkPos mode) const
    {
        PTDatLink temp = nullptr;
        switch (mode)
        {
            case FIRST:
                temp = pFirst;
                break;
            case CURRENT:
                temp = pCurrLink;
                break;
            case LAST:
                temp = pLast;
                break;
        }
        return (temp == nullptr) ? nullptr : temp->GetDatValue();
    }

    int TDatList::Reset()
    {
        pPrevLink = pStop;
        if (IsEmpty()) {
            CurrPos = -1;
            pCurrLink = pStop;
        }
        else {
            CurrPos = 0;
            pCurrLink = pFirst;
        }
        return 1;
    }

    int TDatList::SetCurrentPos(int pos)
    {
        Reset();
        for (int i = 0; i < pos; GoNext());
        return 1;
    }

    int TDatList::GetCurrentPos() const
    {
        return CurrPos;
    }

    int TDatList::IsListEnded() const{
        return (pCurrLink == pStop);
    }

    int TDatList::GoNext()
    {
        if (IsListEnded())
            return 0;
        else {
            pPrevLink = pCurrLink;
            pCurrLink = pCurrLink->GetNextDatLink();
            CurrPos++;
            return 1;
        }
    }

    void TDatList::InsFirst(PTDatValue pVal)
    {
        PTDatLink DatLink = GetLink(pVal, pFirst);
        if (DatLink != nullptr) {
            //DatLink->SetNextLink(pFirst);
            pFirst = DatLink;
            ListLen++;
            if (ListLen == 1) {
                pLast = DatLink;
                Reset();
            }
            else if (CurrPos == 0) {
                pCurrLink = DatLink;
            }
            else
                CurrPos++;
        }
    }

    void TDatList::InsLast(PTDatValue pVal) {
        PTDatLink DatLink = GetLink(pVal, pStop);
        if (DatLink != nullptr)
        {
            if (pLast)
                pLast->SetNextLink(DatLink);
            pLast = DatLink;
            ListLen++;

            if (ListLen == 1)
            {
                pFirst = DatLink;
                //Reset();
            }

            if (IsListEnded())
                pCurrLink = DatLink;
        }
    }

    void TDatList::InsCurrent(PTDatValue pVal ) {
        if ((pCurrLink == pFirst) || IsEmpty())
            InsFirst(pVal);
        else if (IsListEnded())
            InsLast(pVal);
        else
        {
            PTDatLink DatLink = GetLink(pVal, pCurrLink);
            if (DatLink != nullptr)
            {
                pPrevLink->SetNextLink(DatLink);
                DatLink->SetNextLink(pCurrLink);
                pCurrLink = DatLink;
                ListLen++;
            }
        }
    }

    void TDatList::DelFirst() {
        if (!IsEmpty())
        {
            PTDatLink temp = pFirst;
            pFirst = pFirst->GetNextDatLink();
            DelLink(temp);
            ListLen--;
            if (IsEmpty())
            {
                pLast = pStop;
                Reset();
            }
            else if (CurrPos == 0) pCurrLink = pFirst;
            else if (CurrPos == 1) pPrevLink = pStop;
            if (CurrPos) CurrPos--;
        }
    }

    void TDatList::DelCurrent() {
        if (pCurrLink)
        {
            if ((pCurrLink == pFirst) || IsEmpty())
                DelFirst();
            else
            {
                PTDatLink temp = pCurrLink;
                pCurrLink = pCurrLink->GetNextDatLink();
                pPrevLink->SetNextLink(pCurrLink);
                DelLink(temp);
                ListLen--;
                if (pCurrLink == pLast)
                {
                    pLast = pPrevLink;
                    pCurrLink = pStop;
                }
            }
        }
    }

    void TDatList::DelList() {
        while(!IsEmpty())
            DelFirst();
        pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
        CurrPos = -1;
    }

	
#### HeadRing.h:
	
    #pragma once

    #include"tdatlist.h"

    class THeadRing : public TDatList{
      protected:
        PTDatLink pHead;
      public:
        THeadRing ();
       ~THeadRing ();
        virtual void InsFirst( PTDatValue pVal=nullptr ); // после заголовка
        virtual void DelFirst();// удалить первое звено
    };


#### HeadRing.cpp:

    #include "theadring.h"

    THeadRing::THeadRing(): TDatList()
    {
        InsLast();
        pHead = pFirst;
        pStop = pHead;
        Reset();
        ListLen = 0;
        pFirst->SetNextLink(pFirst);
    }

    THeadRing:: ~THeadRing()
    {
        DelList();
        DelLink(pHead);
        pHead = nullptr;
    }

    void THeadRing::InsFirst(PTDatValue pVal)
    {
        TDatList::InsFirst(pVal);
        pHead->SetNextLink(pFirst);
    }

    void THeadRing::DelFirst()
    {
        TDatList::DelFirst();
        pHead->SetNextLink(pFirst);
    }



#### Polinom.h:
    
    #pragma once

    #include<QtCore/qmath.h>
    #include"tmonom.h"
    #include"theadring.h"

    typedef TMonom *PTMonom;

    class TPolinom : public THeadRing {
      public:
        TPolinom ( int monoms[][2]=NULL, int km=0 ); // конструктор полинома из массива «коэффициент-индекс»
        TPolinom (  TPolinom *q );      // конструктор копирования
        PTMonom  GetMonom()  {
            return (PTMonom)GetDatValue();
        }
        TPolinom & operator+( TPolinom &q); // сложение полиномов
        TPolinom & operator=( TPolinom &q); // присваивание
        bool operator==( TPolinom &q); // сравнение
        bool operator!=( TPolinom &q); // сравнение
        int Comput(int x, int y,int z);
        void cleanPol();
    };


#### Polinom.cpp:

    #include "tpolinom.h"

    TPolinom::TPolinom(int monoms[][2], int km)
    {
        PTMonom monom = new TMonom(0, -1);
        pHead->SetDatValue(monom);
        for (int i = 0; i < km; ++i){
            monom = new TMonom(monoms[i][0], monoms[i][1]);
            InsLast(monom);
        }
    }

    TPolinom::TPolinom (  TPolinom *q )      // конструктор копирования
    {
        PTMonom pMonom = new TMonom(0,-1);
        pHead->SetDatValue(pMonom);
        for(q->Reset();!q->IsListEnded();q->GoNext()){
            pMonom = q->GetMonom();
            InsLast(pMonom->GetCopy());
        }
    }
    //*
    TPolinom& TPolinom::operator+(TPolinom &q)
    {
        PTMonom this_mon, q_mon, new_mon;
        q.Reset();
        Reset();
        TPolinom* result = new TPolinom(*this);

        while(true){
            this_mon = result->GetMonom();
            while(true){
                q_mon = q.GetMonom();
                if(q_mon->GetCoeff() == 0)
                    break;
                if(this_mon->Index == q_mon->Index)
                    this_mon->Coeff += q_mon->Coeff;
                q.GoNext();
            }
            result->GoNext();
            q.Reset();
            if(this_mon->Index == -1)
                break;
        }

        Reset();
        q.Reset();
        result->Reset();

        while(true){
            this_mon = result->GetMonom();
            q_mon = q.GetMonom();
            if (this_mon->Index > q_mon->Index)
            {
                new_mon = new TMonom(q_mon->Coeff, q_mon->Index);
                result->InsCurrent(new_mon);
                q.GoNext();
            }
            if(q_mon->Index == this_mon->Index){
                q.GoNext();
                if(q_mon->Index ==-1)
                    break;
                continue;
            }
            else if (q_mon->Index > this_mon->Index)
                if(this_mon->Index == -1){
                    new_mon = new TMonom(q_mon->Coeff, q_mon->Index);
                    result->InsCurrent(new_mon);
                    q.GoNext();
                }
                result->GoNext();
            if (q_mon->Index == -1 || this_mon->Index == -1)
                    break;
        }

        result->cleanPol();
        return *result;
    }

    TPolinom& TPolinom::operator=( TPolinom &q) // присваивание
    {
        DelList();
        for(q.Reset();!q.IsListEnded();q.GoNext()){
            PTMonom pMonom = q.GetMonom();
            InsLast(pMonom->GetCopy());
        }
        return *this;
    }

    bool TPolinom::operator==( TPolinom &q){
        if (q.GetListLength() == 0 && GetListLength() == 0)
            return true;
        if (q.GetListLength() != GetListLength())
            return false;
        for (this->Reset(),q.Reset();!IsListEnded()&&!q.IsListEnded();GoNext(),q.GoNext()){
            if (!(*((PTMonom)GetDatValue())==(*(PTMonom)(q.GetDatValue()))))
                return false;
            }
        return true;
    }

    bool TPolinom::operator!=( TPolinom &q){
        if(q==*this)
            return false;
        else
            return true;
    }

    int TPolinom::Comput(int x, int y, int z){
        long long int res=0;
        for (this->Reset();!IsListEnded();GoNext())
        res+=this->GetMonom()->GetCoeff()*qPow(x,this->GetMonom()->GetIndex()/100)*
                qPow(y,(this->GetMonom()->GetIndex()/10)%10)*
                qPow(z,(this->GetMonom()->GetIndex()%100%10));
        return res;

    }

    void TPolinom::cleanPol(){
        PTMonom this_mon;
        Reset();
        while (true) {
            this_mon = GetMonom();
            if(this_mon->Coeff==0)
                DelCurrent();
            else
                GoNext();
            if(IsListEnded())
                break;
        }
    }

##Демонстрационная программа сложения двух полиномов:
#### mainwindow.h
    
    #ifndef MAINWINDOW_H
    #define MAINWINDOW_H

    #include <QMainWindow>
    #include<iostream>
    #include"tdatlist.h"
    #include"tpolinom.h"

    namespace Ui {
    class MainWindow;
    }

    class MainWindow : public QMainWindow
    {
        Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    private slots:
        void on_pushButton_clicked();

        void on_pushButton_3_clicked();

        void on_pushButton_5_clicked();

        void on_pushButton_6_clicked();

        void on_pushButton_2_clicked();

        void on_pushButton_4_clicked();

        void on_pushButton_7_clicked();

    private:
        Ui::MainWindow *ui;
        TPolinom Pol1,Pol2,Sum;
    };

    #endif // MAINWINDOW_H

#### mainwindow.cpp
    
    #include "mainwindow.h"
    #include "ui_mainwindow.h"

    MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow)
    {
        ui->setupUi(this);
        Pol1.Reset();
        Pol2.Reset();
    }

    MainWindow::~MainWindow()
    {
        delete ui;
    }

    //add
    void MainWindow::on_pushButton_3_clicked()
    {
        TPolinom t =new TPolinom(Pol2);
        Sum = t + Pol1;
        Sum.Reset();
        /*
        while(true){
            std::cout<<Sum.GetMonom()->GetCoeff()<<std::endl;
            std::cout<<Sum.GetMonom()->GetIndex()<<std::endl;
            Sum.GoNext();
            if(Sum.IsListEnded())
                break;
        }*/
    }

    //pol1
    void MainWindow::on_pushButton_5_clicked()
    {
        int coeff = ui->Coeff_2->value();
        int x = ui->X_2->value();
        int y = ui->Y_2->value();
        int z = ui->Z_2->value();
        z+=y*10;
        z+=x*100;
        PTMonom monom = new TMonom(coeff, z);
        Pol1.InsLast(monom);
        std::cout<<"Pol1"<<std::endl;
        std::cout<<monom->GetCoeff()<<std::endl;
        std::cout<<monom->GetIndex()<<std::endl;
    }
    //pol1 c
    void MainWindow::on_pushButton_clicked()
    {
        int x = ui->spinBox->value();
        int y = ui->spinBox_2->value();
        int z = ui->spinBox_3->value();
        ui->lineEdit->setText("Pol1: "+QString::number(Pol1.Comput(x,y,z)));
        std::cout<<Pol1.Comput(x,y,z)<<std::endl;
    }
    //Pol2
    void MainWindow::on_pushButton_6_clicked()
    {
        int coeff = ui->Coeff->value();
        int x = ui->X->value();
        int y = ui->Y->value();
        int z = ui->Z->value();
        z+=y*10;
        z+=x*100;
        PTMonom monom = new TMonom(coeff, z);
        Pol2.InsLast(monom);
        std::cout<<"pol2"<<std::endl;
        std::cout<<monom->GetCoeff()<<std::endl;
        std::cout<<monom->GetIndex()<<std::endl;
    }
    //pol2 c
    void MainWindow::on_pushButton_2_clicked()
    {
        int x = ui->spinBox->value();
        int y = ui->spinBox_2->value();
        int z = ui->spinBox_3->value();
        ui->lineEdit->setText("Pol2: "+QString::number(Pol2.Comput(x,y,z)));
        std::cout<<Pol2.Comput(x,y,z)<<std::endl;
    }
    //print
    void MainWindow::on_pushButton_4_clicked()
    {
        int x=0,y=0,z=0,co=0,temp_i=0;
        QString temp_p1,temp_p2,temp_s,temp;
        ui->plainTextEdit->clear();
        ui->plainTextEdit_2->clear();
        ui->plainTextEdit_3->clear();
        Pol1.Reset();
        Pol2.Reset();
        Sum.Reset();

        while (true) {
            if(Pol1.IsListEnded())
                break;

            temp_i=Pol1.GetMonom()->GetIndex();
            z = temp_i%100%10;
            y = temp_i%100/10;
            x = temp_i/100;
            co=Pol1.GetMonom()->GetCoeff();

            if(co!=0){
                temp = QString::number(co);
                temp_p1+=temp;
                if(x!=0){
                    temp_p1+="*";
                    temp_p1+="x^";
                    temp = QString::number(x);
                    temp_p1+=temp;
                }
                if(y!=0){
                    temp_p1+="*";
                    temp_p1+="y^";
                    temp = QString::number(y);
                    temp_p1+=temp;
                }
                if(z!=0){
                    temp_p1+="*";
                    temp_p1+="z^";
                    temp = QString::number(z);
                    temp_p1+=temp;
                }
                temp_p1+="\n";
            }
            Pol1.GoNext();
        }

        while (true) {
            if(Pol2.IsListEnded())
                break;

            temp_i=Pol2.GetMonom()->GetIndex();
            z = temp_i%100%10;
            y = temp_i%100/10;
            x = temp_i/100;
            co=Pol2.GetMonom()->GetCoeff();

            if(co!=0){
                temp = QString::number(co);
                temp_p2+=temp;
                if(x!=0){
                    temp_p2+="*";
                    temp_p2+="x^";
                    temp = QString::number(x);
                    temp_p2+=temp;
                }
                if(y!=0){
                    temp_p2+="*";
                    temp_p2+="y^";
                    temp = QString::number(y);
                    temp_p2+=temp;
                }
                if(z!=0){
                    temp_p2+="*";
                    temp_p2+="z^";
                    temp = QString::number(z);
                    temp_p2+=temp;
                }
                temp_p2+="\n";

            }
            Pol2.GoNext();
        }

        while (true) {
            if(Sum.IsListEnded())
                break;

            temp_i=Sum.GetMonom()->GetIndex();
            z = temp_i%100%10;
            y = temp_i%100/10;
            x = temp_i/100;
            co=Sum.GetMonom()->GetCoeff();

            if(co!=0){
                temp = QString::number(co);
                temp_s+=temp;
                if(x!=0){
                    temp_s+="*";
                    temp_s+="x^";
                    temp = QString::number(x);
                    temp_s+=temp;
                }
                if(y!=0){
                    temp_s+="*";
                    temp_s+="y^";
                    temp = QString::number(y);
                    temp_s+=temp;
                }
                if(z!=0){
                    temp_s+="*";
                    temp_s+="z^";
                    temp = QString::number(z);
                    temp_s+=temp;
                }
                temp_s+="\n";

            }
            Sum.GoNext();
        }
        std::cout<<"pol: "<<temp_p1.toStdString()<<std::endl;
        std::cout<<"pol2: "<<temp_p2.toStdString()<<std::endl;
        std::cout<<"sum: "<<temp_s.toStdString()<<std::endl;

        ui->plainTextEdit->insertPlainText(temp_p1);
        ui->plainTextEdit_2->insertPlainText(temp_p2);
        ui->plainTextEdit_3->insertPlainText(temp_s);
    }
    //del
    void MainWindow::on_pushButton_7_clicked()
    {
        Pol1.DelList();
        Pol2.DelList();
        Sum.DelList();
        ui->plainTextEdit->clear();
        ui->plainTextEdit_2->clear();
        ui->plainTextEdit_3->clear();
    }

##### Результат запуска демонстрационной программы:

![](demo.png)


### 3. Проверка работоспособности при помощи Google Test Framework

Данные классы были протестированы с помощью фреймворка **Google Test**.

### list_test_kit.cpp
    
    #include<iostream>
    #include "gtest.h"
    #include "tpolinom.h"

    TEST(TPolinom, can_create_polinom)
    {
        EXPECT_NO_THROW(TPolinom Pol);
    }

    TEST(TPolinom, copy_constructer_workOK)
    {
        TPolinom Pol1;

        TPolinom Pol2 = Pol1;

        EXPECT_TRUE(Pol1 == Pol2);
    }

    TEST(TPolinom, copied_polinom_has_its_own_adress)
    {
        TPolinom Pol1;

        TPolinom Pol2 = Pol1;

        EXPECT_NE(&Pol1,&Pol2);
    }

    TEST(TPolinom, can_compare_polynoms)
    {
        const int size = 3;
        int mon[][2] = { { 1, 3 },{ 2, 4 },{ 2, 100 } };
        TPolinom Pol1(mon, size);
        TPolinom Pol2(mon, size);

        EXPECT_TRUE(Pol1 == Pol2);
    }

    TEST(TPolinom, can_assign_polynoms)
    {
        // Arrange
        const int size = 2;
        int mon[][2] = { { 5, 3 },{ 2, 4 } };
        TPolinom Pol1(mon, size);
        TPolinom Pol2;

        Pol2 = Pol1;

        EXPECT_TRUE(Pol1 == Pol2);
    }

    TEST(TPolinom, can_assign_many_polynoms)
    {
        const int size = 5;
        int mon[][2] = { { 5, 3 },{ 2, 4 },{ 5, 3 },{ 2, 4 },{ 5, 3 } };

        TPolinom Pol1(mon, size);
        TPolinom Pol2,Pol3;
        Pol3 = Pol2 = Pol1;

        bool _check = (Pol1 == Pol2) && (Pol2 == Pol3) && (Pol1 == Pol3);

        EXPECT_TRUE(_check);
    }

    TEST(TPolinom, can_add_simple_polynoms)
    {
        const int size1 = 3;
        const int size2 = 4;
        int mon1[][2] = { { 5, 2 },{ 8, 3 },{ 9, 4 } };
        int mon2[][2] = { { 1, 1 },{ -8, 3 },{ 1, 4 },{ 2, 5 } };
        // 5z^2+8z^3+9z^4
        TPolinom Pol1(mon1, size1);
        // z-8z^3+z^4+2z^5
        TPolinom Pol2(mon2, size2);

        TPolinom Pol= Pol1 + Pol2;

        const int expected_size = 4;
        int expected_mon[][2] = { { 1, 1 },{ 5, 2 },{ 10, 4 },{ 2, 5 } };
        // z+5z^2+10z^4+2z^5
        TPolinom check_Pol(expected_mon, expected_size);

        EXPECT_TRUE(Pol == check_Pol);
    }

    TEST(TPolinom, can_create_polinom_from_one_monom)
    {
        int mon[][2] = { { 1, 3 } };

        TPolinom Pol(mon, 1);

        TMonom res(1, 3);
        ASSERT_TRUE(res == (TMonom)(*Pol.GetMonom()));
    }

    TEST(TPolinom, can_create_polinom_from_two_monoms)
    {
        const int size = 2;
        int mon[][2] = { { 9, 999 },{ 1, 111 } };

        TPolinom Pol(mon, size);

        TMonom monoms[size];
        for (int i = 0; i < size; i++)
            monoms[i] = TMonom(mon[i][0], mon[i][1]);
        for (int i = 0; i < size; i++, Pol.GoNext())
            EXPECT_TRUE(monoms[i]== *Pol.GetMonom());
    }

    TEST(TPolinom, can_create_pol_from_many_monoms)
    {
        const int size = 10;
        int mon[][2] = { { 1, 3 },{ 2, 4 },{ 2, 100 },{ 3, 110 },
        { 5, 150 },{ 6, 302 },{ 3, 400 },{ 2, 500 },{ 7 ,800 },{ 2, 888 } };

        TPolinom Pol(mon, size);

        TMonom monoms[size];
        for (int i = 0; i < size; i++)
            monoms[i] = TMonom(mon[i][0], mon[i][1]);
        for (int i = 0; i < size; i++, Pol.GoNext())
            EXPECT_TRUE(monoms[i] == *Pol.GetMonom());
    }

    TEST(TPolinom, can_assign_empty_polynom)
    {
        TPolinom Pol1;
        TPolinom Pol2;

        Pol2 = Pol1;

        EXPECT_TRUE(Pol1 == Pol2);
    }

    TEST(TPolinom, can_add_up_linear_polynoms)
    {
        const int size = 1;
        int mon1[][2] = { { 2, 1 } };
        int mon2[][2] = { { 1, 1 } };
        // 2z
        TPolinom Pol1(mon1, size);
        // z
        TPolinom Pol2(mon2, size);

        TPolinom Pol = Pol1 + Pol2;

        const int expected_size = 1;
        int expected_mon[][2] = { { 3, 1 } };
        // z+2z^2
        TPolinom expected_Pol(expected_mon, expected_size);

        EXPECT_TRUE(Pol == expected_Pol);
    }

    TEST(TPolinom, can_add_up_polynoms)
    {
        const int size1 = 5;
        const int size2 = 4;
        int mon1[][2] = { { 5, 213 },{ 8, 321 },{ 10, 432 },{ -21, 500 },{ 10, 999 } };
        int mon2[][2] = { { 15, 0 },{ -8, 321 },{ 1, 500 },{ 20, 702 } };
        // 5x^2yz^3+8x^3y^2z+10x^4y^3z^2-21x^5+10x^9y^9z^9
        TPolinom Pol1(mon1, size1);
        // 15-8x^3y^2z+x^5+20x^7z^2
        TPolinom Pol2(mon2, size2);

        TPolinom Pol = Pol1 + Pol2;

        const int expected_size = 6;
        int expected_mon[][2] = { { 15, 0 },{ 5, 213 },{ 10, 432 },{ -20, 500 },{ 20, 702 },{ 10, 999 } };
        // 15+5x^2yz^3+10x^4y^3z^2-20x^5+20x^7z^2+10x^9y^9z^9
        TPolinom expected_Pol(expected_mon, expected_size);

        EXPECT_TRUE(Pol == expected_Pol);
    }

    TEST(TPolinom, can_add_up_many_polynoms)
    {
        const int size1 = 3;
        const int size2 = 4;
        const int size3 = 3;
        int mon1[][2] = { { 5, 2 },{ 8, 3 },{ 9, 4 } };
        int mon2[][2] = { { 1, 1 },{ -8, 3 },{ 1, 4 },{ 2, 5 } };
        int mon3[][2] = { { 10, 0 },{ 2, 3 },{ 8, 5 } };
        // 5z^2+8z^3+9z^4
        TPolinom Pol1(mon1, size1);
        // z-8z^3+z^4+2z^5
        TPolinom Pol2(mon2, size2);
        // 10+2z^3+8z^5
        TPolinom Pol3(mon3, size3);

        TPolinom Pol = Pol1 + Pol2 + Pol3;

        const int expected_size = 6;
        int expected_mon[][2] = { { 10, 0 },{ 1, 1 },{ 5, 2 },{ 2, 3 },{ 10, 4 },{ 10, 5 } };
        // z+5z^2+10z^4+2z^5
        TPolinom expected_Pol(expected_mon, expected_size);


        EXPECT_TRUE(Pol == expected_Pol);
    }

    TEST(TPolinom, can_calculate_the_polinom_with_different_monoms)
    {
        int size = 3;
        int mon[][2] = { { 1, 1 },{ 2, 10 },{ 3, 100 } };
        TPolinom Pol(mon, size);

        EXPECT_EQ(Pol.Comput(1, 1, 1), 6);
    }

    TEST(TPolinom, can_calculate_the_polinom)
    {
        int size = 3;
        int mon[][2] = { { 1, 111 },{ -2, 111 },{ 3, 111 } };
        TPolinom Pol(mon, size);

        EXPECT_EQ(Pol.Comput(1, 1, 1), 2);
    }

    TEST(TPolinom, can_calculate_the_polinom1)
    {
        int size = 3;
        int mon[][2] = { { 1, 111 },{ -1, 111 },{ 2, 11 } };
        TPolinom Pol(mon, size);

        EXPECT_EQ(Pol.Comput(1, 1, 1), 2);
    }

    TEST(TPolinom, can_calculate_the_empty_polinom)
    {
        TPolinom Pol;

        EXPECT_EQ(Pol.Comput(1, 1, 1), 0);
    }

    TEST(TPolinom, Pol_isEmpty){
        TPolinom Pol;
        ASSERT_TRUE(Pol.IsEmpty());
    }
    //list
    TEST(TPolinom, length_tru)
    {
        const int size = 3;
        int monoms[][2]={{21,123},{123,321},{123,321}};
        TPolinom Pol(monoms,size);
        ASSERT_TRUE(Pol.GetListLength()==size);
    }
    TEST(TPolinom, can_get_all)
    {
        const int size = 3;
        int monoms[][2]={{21,123},{123,321},{123,321}};
        TPolinom Pol(monoms,size);
        Pol.Reset();
        Pol.GoNext();
        ASSERT_TRUE(Pol.GetCurrentPos() == 1);
    }
    //методы вставки перед первым, после текущего и последнего звеньев;
    TEST(TPolinom, can_insert)
    {
        const int size = 1;
        TMonom testmonom(999,999);
        int monoms[][2]={{21,123}};
        TPolinom Pol(monoms,size);
        Pol.Reset();
        Pol.InsFirst(&testmonom);
        ASSERT_TRUE(Pol.GetMonom()==&testmonom);
        Pol.GoNext();
        Pol.InsCurrent(&testmonom);
        ASSERT_TRUE(Pol.GetMonom()==&testmonom);
        Pol.InsLast(&testmonom);
        Pol.GoNext();
        Pol.GoNext();
        ASSERT_TRUE(Pol.GetMonom()==&testmonom);
    }

    //	методы удаления первого и текущего звена.
    TEST(TPolinom, can_delete)
    {
        const int size = 3;
        int monoms[][2]={{1,123},{2,133},{3,143}};
        TPolinom Pol(monoms,size);
        TPolinom Pol2(monoms,size);
        Pol.Reset();
        Pol.DelFirst();
        Pol2.Reset();
        Pol2.GoNext();
        ASSERT_TRUE(*Pol.GetMonom()==*Pol2.GetMonom());
        Pol2.Reset();
        Pol2.DelCurrent();
        ASSERT_TRUE(*Pol.GetMonom()==*Pol2.GetMonom());
        Pol.DelList();
        Pol2.DelList();
        ASSERT_TRUE(Pol.IsEmpty()==Pol2.IsEmpty());
    }

**Результат**

![](tests.png)



## Выводы

В ходе выполнения данной работы были получены навыки создания структуры данных список (TDatList) и организации работы с полиномами на основе списков (TPolinom). Также было создано GUI приложение на основе QT.
