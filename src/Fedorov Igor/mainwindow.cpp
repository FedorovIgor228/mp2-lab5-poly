#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Pol1.Reset();
    Pol2.Reset();
}

MainWindow::~MainWindow()
{
    delete ui;
}

//add
void MainWindow::on_pushButton_3_clicked()
{
    TPolinom t =new TPolinom(Pol2);
    Sum = t + Pol1;
    Sum.Reset();
    /*
    while(true){
        std::cout<<Sum.GetMonom()->GetCoeff()<<std::endl;
        std::cout<<Sum.GetMonom()->GetIndex()<<std::endl;
        Sum.GoNext();
        if(Sum.IsListEnded())
            break;
    }*/
}

//pol1
void MainWindow::on_pushButton_5_clicked()
{
    int coeff = ui->Coeff_2->value();
    int x = ui->X_2->value();
    int y = ui->Y_2->value();
    int z = ui->Z_2->value();
    z+=y*10;
    z+=x*100;
    PTMonom monom = new TMonom(coeff, z);
    Pol1.InsLast(monom);
    std::cout<<"Pol1"<<std::endl;
    std::cout<<monom->GetCoeff()<<std::endl;
    std::cout<<monom->GetIndex()<<std::endl;
}
//pol1 c
void MainWindow::on_pushButton_clicked()
{
    int x = ui->spinBox->value();
    int y = ui->spinBox_2->value();
    int z = ui->spinBox_3->value();
    ui->lineEdit->setText("Pol1: "+QString::number(Pol1.Comput(x,y,z)));
    std::cout<<Pol1.Comput(x,y,z)<<std::endl;
}
//Pol2
void MainWindow::on_pushButton_6_clicked()
{
    int coeff = ui->Coeff->value();
    int x = ui->X->value();
    int y = ui->Y->value();
    int z = ui->Z->value();
    z+=y*10;
    z+=x*100;
    PTMonom monom = new TMonom(coeff, z);
    Pol2.InsLast(monom);
    std::cout<<"pol2"<<std::endl;
    std::cout<<monom->GetCoeff()<<std::endl;
    std::cout<<monom->GetIndex()<<std::endl;
}
//pol2 c
void MainWindow::on_pushButton_2_clicked()
{
    int x = ui->spinBox->value();
    int y = ui->spinBox_2->value();
    int z = ui->spinBox_3->value();
    ui->lineEdit->setText("Pol2: "+QString::number(Pol2.Comput(x,y,z)));
    std::cout<<Pol2.Comput(x,y,z)<<std::endl;
}
//print
void MainWindow::on_pushButton_4_clicked()
{
    int x=0,y=0,z=0,co=0,temp_i=0;
    QString temp_p1,temp_p2,temp_s,temp;
    ui->plainTextEdit->clear();
    ui->plainTextEdit_2->clear();
    ui->plainTextEdit_3->clear();
    Pol1.Reset();
    Pol2.Reset();
    Sum.Reset();

    while (true) {
        if(Pol1.IsListEnded())
            break;

        temp_i=Pol1.GetMonom()->GetIndex();
        z = temp_i%100%10;
        y = temp_i%100/10;
        x = temp_i/100;
        co=Pol1.GetMonom()->GetCoeff();

        if(co!=0){
            temp = QString::number(co);
            temp_p1+=temp;
            if(x!=0){
                temp_p1+="*";
                temp_p1+="x^";
                temp = QString::number(x);
                temp_p1+=temp;
            }
            if(y!=0){
                temp_p1+="*";
                temp_p1+="y^";
                temp = QString::number(y);
                temp_p1+=temp;
            }
            if(z!=0){
                temp_p1+="*";
                temp_p1+="z^";
                temp = QString::number(z);
                temp_p1+=temp;
            }
            temp_p1+="\n";
        }
        Pol1.GoNext();
    }

    while (true) {
        if(Pol2.IsListEnded())
            break;

        temp_i=Pol2.GetMonom()->GetIndex();
        z = temp_i%100%10;
        y = temp_i%100/10;
        x = temp_i/100;
        co=Pol2.GetMonom()->GetCoeff();

        if(co!=0){
            temp = QString::number(co);
            temp_p2+=temp;
            if(x!=0){
                temp_p2+="*";
                temp_p2+="x^";
                temp = QString::number(x);
                temp_p2+=temp;
            }
            if(y!=0){
                temp_p2+="*";
                temp_p2+="y^";
                temp = QString::number(y);
                temp_p2+=temp;
            }
            if(z!=0){
                temp_p2+="*";
                temp_p2+="z^";
                temp = QString::number(z);
                temp_p2+=temp;
            }
            temp_p2+="\n";

        }
        Pol2.GoNext();
    }

    while (true) {
        if(Sum.IsListEnded())
            break;

        temp_i=Sum.GetMonom()->GetIndex();
        z = temp_i%100%10;
        y = temp_i%100/10;
        x = temp_i/100;
        co=Sum.GetMonom()->GetCoeff();

        if(co!=0){
            temp = QString::number(co);
            temp_s+=temp;
            if(x!=0){
                temp_s+="*";
                temp_s+="x^";
                temp = QString::number(x);
                temp_s+=temp;
            }
            if(y!=0){
                temp_s+="*";
                temp_s+="y^";
                temp = QString::number(y);
                temp_s+=temp;
            }
            if(z!=0){
                temp_s+="*";
                temp_s+="z^";
                temp = QString::number(z);
                temp_s+=temp;
            }
            temp_s+="\n";

        }
        Sum.GoNext();
    }
    std::cout<<"pol: "<<temp_p1.toStdString()<<std::endl;
    std::cout<<"pol2: "<<temp_p2.toStdString()<<std::endl;
    std::cout<<"sum: "<<temp_s.toStdString()<<std::endl;

    ui->plainTextEdit->insertPlainText(temp_p1);
    ui->plainTextEdit_2->insertPlainText(temp_p2);
    ui->plainTextEdit_3->insertPlainText(temp_s);
}
//del
void MainWindow::on_pushButton_7_clicked()
{
    Pol1.DelList();
    Pol2.DelList();
    Sum.DelList();
    ui->plainTextEdit->clear();
    ui->plainTextEdit_2->clear();
    ui->plainTextEdit_3->clear();
}
