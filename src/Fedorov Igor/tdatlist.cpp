#include "tdatlist.h"

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
    return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)
{
    if (pLink != nullptr)
    {
        if (pLink->pValue != nullptr)
            delete pLink->pValue;
        delete pLink;
    }
}

TDatList::TDatList(): ListLen(0)
{
    pFirst = pLast = pStop = nullptr;
    Reset();
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
    PTDatLink temp = nullptr;
    switch (mode)
    {
        case FIRST:
            temp = pFirst;
            break;
        case CURRENT:
            temp = pCurrLink;
            break;
        case LAST:
            temp = pLast;
            break;
    }
    return (temp == nullptr) ? nullptr : temp->GetDatValue();
}

int TDatList::Reset()
{
    pPrevLink = pStop;
    if (IsEmpty()) {
        CurrPos = -1;
        pCurrLink = pStop;
    }
    else {
        CurrPos = 0;
        pCurrLink = pFirst;
    }
    return 1;
}

int TDatList::SetCurrentPos(int pos)
{
    Reset();
    for (int i = 0; i < pos; GoNext());
    return 1;
}

int TDatList::GetCurrentPos() const
{
    return CurrPos;
}

int TDatList::IsListEnded() const{
    return (pCurrLink == pStop);
}

int TDatList::GoNext()
{
    if (IsListEnded())
        return 0;
    else {
        pPrevLink = pCurrLink;
        pCurrLink = pCurrLink->GetNextDatLink();
        CurrPos++;
        return 1;
    }
}

void TDatList::InsFirst(PTDatValue pVal)
{
    PTDatLink DatLink = GetLink(pVal, pFirst);
    if (DatLink != nullptr) {
        //DatLink->SetNextLink(pFirst);
        pFirst = DatLink;
        ListLen++;
        if (ListLen == 1) {
            pLast = DatLink;
            Reset();
        }
        else if (CurrPos == 0) {
            pCurrLink = DatLink;
        }
        else
            CurrPos++;
    }
}

void TDatList::InsLast(PTDatValue pVal) {
    PTDatLink DatLink = GetLink(pVal, pStop);
    if (DatLink != nullptr)
    {
        if (pLast)
            pLast->SetNextLink(DatLink);
        pLast = DatLink;
        ListLen++;

        if (ListLen == 1)
        {
            pFirst = DatLink;
            //Reset();
        }

        if (IsListEnded())
            pCurrLink = DatLink;
    }
}

void TDatList::InsCurrent(PTDatValue pVal ) {
    if ((pCurrLink == pFirst) || IsEmpty())
        InsFirst(pVal);
    else if (IsListEnded())
        InsLast(pVal);
    else
    {
        PTDatLink DatLink = GetLink(pVal, pCurrLink);
        if (DatLink != nullptr)
        {
            pPrevLink->SetNextLink(DatLink);
            DatLink->SetNextLink(pCurrLink);
            pCurrLink = DatLink;
            ListLen++;
        }
    }
}

void TDatList::DelFirst() {
    if (!IsEmpty())
    {
        PTDatLink temp = pFirst;
        pFirst = pFirst->GetNextDatLink();
        DelLink(temp);
        ListLen--;
        if (IsEmpty())
        {
            pLast = pStop;
            Reset();
        }
        else if (CurrPos == 0) pCurrLink = pFirst;
        else if (CurrPos == 1) pPrevLink = pStop;
        if (CurrPos) CurrPos--;
    }
}

void TDatList::DelCurrent() {
    if (pCurrLink)
    {
        if ((pCurrLink == pFirst) || IsEmpty())
            DelFirst();
        else
        {
            PTDatLink temp = pCurrLink;
            pCurrLink = pCurrLink->GetNextDatLink();
            pPrevLink->SetNextLink(pCurrLink);
            DelLink(temp);
            ListLen--;
            if (pCurrLink == pLast)
            {
                pLast = pPrevLink;
                pCurrLink = pStop;
            }
        }
    }
}

void TDatList::DelList() {
    while(!IsEmpty())
        DelFirst();
    pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
    CurrPos = -1;
}
