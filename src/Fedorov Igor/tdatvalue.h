#ifndef TDATVALUE_H
#define TDATVALUE_H

#include<iostream>

class TDatValue{
public:
    virtual TDatValue* GetCopy() = 0; // создание копии
    ~TDatValue() {}
};

typedef TDatValue *PTDatValue;
#endif // TDATVALUE_H
