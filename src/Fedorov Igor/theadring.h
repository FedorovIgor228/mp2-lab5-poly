#pragma once

#include"tdatlist.h"

class THeadRing : public TDatList{
  protected:
    PTDatLink pHead;
  public:
    THeadRing ();
   ~THeadRing ();
    virtual void InsFirst( PTDatValue pVal=nullptr ); // после заголовка
    virtual void DelFirst();// удалить первое звено
};

