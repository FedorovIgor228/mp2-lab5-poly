#pragma once

#include "ostream"
#include"tdatlink.h"

class TMonom;
typedef TMonom* PTMonom;

class TMonom : public TDatValue  {
protected:
    int Coeff; // коэффициент монома
    int Index; // индекс (свертка степеней)
  public:
    TMonom ( int cval=1, int ival=0 ) {
      Coeff=cval;
      Index=ival;
    }
    virtual TDatValue * GetCopy() // изготовить копию
    {
        TDatValue *temp = new TMonom(Coeff,Index);
        return temp;
    }
    void SetCoeff(int cval) {
        Coeff=cval;
    }
    int  GetCoeff(void)     {
        return Coeff;
    }
    void SetIndex(int ival) {
        Index=ival;
    }
    int  GetIndex(void)     {
        return Index;
    }
    TMonom& operator=(const TMonom &tm) {
      Coeff=tm.Coeff; Index=tm.Index;
      return *this;
    }
    bool operator==(const TMonom &tm) {
      return (this->Coeff==tm.Coeff) && (this->Index==tm.Index);
    }
    int operator<(const TMonom &tm) {
      return Index<tm.Index;
    }
    friend std::ostream& operator <<(std::ostream &os,TMonom &tm){
        os << tm.Coeff << " " << tm.Index;
        return os;
    }
    friend class TPolinom;
};

