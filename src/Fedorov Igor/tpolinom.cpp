#include "tpolinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
    PTMonom monom = new TMonom(0, -1);
    pHead->SetDatValue(monom);
    for (int i = 0; i < km; ++i){
        monom = new TMonom(monoms[i][0], monoms[i][1]);
        InsLast(monom);
    }
}

TPolinom::TPolinom (  TPolinom *q )      // конструктор копирования
{
    PTMonom pMonom = new TMonom(0,-1);
    pHead->SetDatValue(pMonom);
    for(q->Reset();!q->IsListEnded();q->GoNext()){
        pMonom = q->GetMonom();
        InsLast(pMonom->GetCopy());
    }
}
//*
TPolinom& TPolinom::operator+(TPolinom &q)
{
    PTMonom this_mon, q_mon, new_mon;
    q.Reset();
    Reset();
    TPolinom* result = new TPolinom(*this);

    while(true){
        this_mon = result->GetMonom();
        while(true){
            q_mon = q.GetMonom();
            if(q_mon->GetCoeff() == 0)
                break;
            if(this_mon->Index == q_mon->Index)
                this_mon->Coeff += q_mon->Coeff;
            q.GoNext();
        }
        result->GoNext();
        q.Reset();
        if(this_mon->Index == -1)
            break;
    }

    Reset();
    q.Reset();
    result->Reset();

    while(true){
        this_mon = result->GetMonom();
        q_mon = q.GetMonom();
        if (this_mon->Index > q_mon->Index)
        {
            new_mon = new TMonom(q_mon->Coeff, q_mon->Index);
            result->InsCurrent(new_mon);
            q.GoNext();
        }
        if(q_mon->Index == this_mon->Index){
            q.GoNext();
            if(q_mon->Index ==-1)
                break;
            continue;
        }
        else if (q_mon->Index > this_mon->Index)
            if(this_mon->Index == -1){
                new_mon = new TMonom(q_mon->Coeff, q_mon->Index);
                result->InsCurrent(new_mon);
                q.GoNext();
            }
            result->GoNext();
        if (q_mon->Index == -1 || this_mon->Index == -1)
                break;
    }

    result->cleanPol();
    return *result;
}

TPolinom& TPolinom::operator=( TPolinom &q) // присваивание
{
    DelList();
    for(q.Reset();!q.IsListEnded();q.GoNext()){
        PTMonom pMonom = q.GetMonom();
        InsLast(pMonom->GetCopy());
    }
    return *this;
}

bool TPolinom::operator==( TPolinom &q){
    if (q.GetListLength() == 0 && GetListLength() == 0)
        return true;
    if (q.GetListLength() != GetListLength())
        return false;
    for (this->Reset(),q.Reset();!IsListEnded()&&!q.IsListEnded();GoNext(),q.GoNext()){
        if (!(*((PTMonom)GetDatValue())==(*(PTMonom)(q.GetDatValue()))))
            return false;
        }
    return true;
}

bool TPolinom::operator!=( TPolinom &q){
    if(q==*this)
        return false;
    else
        return true;
}

int TPolinom::Comput(int x, int y, int z){
    long long int res=0;
    for (this->Reset();!IsListEnded();GoNext())
    res+=this->GetMonom()->GetCoeff()*qPow(x,this->GetMonom()->GetIndex()/100)*
            qPow(y,(this->GetMonom()->GetIndex()/10)%10)*
            qPow(z,(this->GetMonom()->GetIndex()%100%10));
    return res;

}

void TPolinom::cleanPol(){
    PTMonom this_mon;
    Reset();
    while (true) {
        this_mon = GetMonom();
        if(this_mon->Coeff==0)
            DelCurrent();
        else
            GoNext();
        if(IsListEnded())
            break;
    }
}
