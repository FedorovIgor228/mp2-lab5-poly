#pragma once

#include<QtCore/qmath.h>
#include"tmonom.h"
#include"theadring.h"

typedef TMonom *PTMonom;

class TPolinom : public THeadRing {
  public:
    TPolinom ( int monoms[][2]=NULL, int km=0 ); // конструктор полинома из массива «коэффициент-индекс»
    TPolinom (  TPolinom *q );      // конструктор копирования
    PTMonom  GetMonom()  {
        return (PTMonom)GetDatValue();
    }
    TPolinom & operator+( TPolinom &q); // сложение полиномов
    TPolinom & operator=( TPolinom &q); // присваивание
    bool operator==( TPolinom &q); // сравнение
    bool operator!=( TPolinom &q); // сравнение
    int Comput(int x, int y,int z);
    void cleanPol();
};
