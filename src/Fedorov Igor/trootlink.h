#ifndef TROOTLINK_H
#define TROOTLINK_H

#include"tdatvalue.h"
#include<iostream>

class TRootLink;
typedef TRootLink *PTRootLink;


class TRootLink {
  protected:
    PTRootLink  pNext;  // указатель на следующее звено
  public:
    TRootLink ( PTRootLink pN = nullptr ) {
        pNext = pN;
    }
    PTRootLink  GetNextLink () {
        return  pNext;
    }
    void SetNextLink ( PTRootLink  pLink ) {
        pNext  = pLink;
    }
    void InsNextLink ( PTRootLink  pLink ) {
      PTRootLink ptr = pNext;
      pNext  = pLink;
      if (pLink != nullptr)
          pLink->pNext = ptr;
    }
    virtual void SetDatValue ( PTDatValue pVal ) = 0;
    virtual PTDatValue GetDatValue () = 0;

    friend class TDatList;
};

#endif // TROOTLINK_H
